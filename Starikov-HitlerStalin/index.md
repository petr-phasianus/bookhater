Autor nejdříve vytvoří jakousi pravdu 

> Již jsme si vysvětlili, že jakékoli přidělování prostředků politickým stranám vždy sleduje dosažení nějakého cíle. (str. 36). 

Dále cituje zprávu Joachima Festa, jak Hitler přivezl z Curychu plný kufr švýcarských franků a vydává to za snahu zamaskovat původ těchto peněz:

> A někdo se nás snaží přesvědčit, že to udělali Švýcaři! (str. 36)

Následuje několik odstavců vysvětlujících, že k převodům takovýchto financí se vždy používají neutrální země (pro nás je zajímavé, že do toho autor zaplete i Českoskoslovensko), s poukazem na to jak většina historiků na to skočí. Pak je uveden na scénu Kurt W. Ludecke, kterému se prý říkalo "černý kůň", který

> také zajišťoval z dosud neznámých a zjevně cizích zdrojů nezanedbatelné prostředky -- financoval vlastní odíl SA, 
> čítající přes 50 mužů. (str. 38)

A je konstatováno že tento muž je novinář blízký NSDAP. 

> Není se čemu divit "Černý kůň" je synonymum slova "zpravodajec" nebo "agent". Dopisovatel - to je oblíbená legenda pro bezpečností důstojníky v utajení. (str. 38)

Odstavec končí oblíbenými třemi tečkami

> [...] Kurt Ludecke je vyslán do USA...

Autor pak přechází k vyvrácení, nebo spíše redukci, teorie o tom, jak Hitlera financovala Francie a pokračuje popisem zbídačeného Německa, což srovnává se situací po Perestrojce a rozpadu Sovětského svazu, přičemž ruská situace byla 
v porovnání s tou německou prý "rajskou zahradou. A přes tento popis německé bídy se autor dostává k Main Kampf. 
A hojně tuto knihu cituje, zvláště pak místa, kde Hitler píše o Anglii, cituje jak Hitler Angii velebí, i Hitlerovo
tvrzení, že Německu zbývají jen dva spojenci Anglie a Itálie (autor má tuto konstatující větu tučně a v uzovkách, tak to snad má být citát, ale není ozdrojován). Z čehož pak autor usuzuje, že Hitler to nepsal pro německý lid, 
nýbrž pro angličany, aby jim ukázal, jak je pro ně perspektivní. To se opakuje několik stránek, až do konce kapitoly.

Další kapitolu vysvětluje název: _Lev Trocki - Otec německého nacismu_, autor v ní dává do souvislosti dění v Německu 
a Říjnovou revoluci v Rusku:

> Revoluce v Německu a Rusku zorganizovala britská zpravodajská služba za významné pomoci Spojených států a Francie. (str. 60, tučně)

Tímto kapitola v podstatě začíná, a pokračuje líčením vnitropolické situace v Rusku v mezinárodním kontextu - zde je to 
zajímavé, neboť já znám toto období vykládané marxisticky, a od marxistického pohledu se autor distancuje, rozebírá vnitřní vztahy Stalina a Trockého a dochází k názoru, že právě Trocki také financoval Hitlera spletitým systémem půjček. Nakonec i sám Trocki byl snad příjemcem zahraničních dotací, ba dokonce: 

> Na zapáleném revolucionáři je zajímavá i naprostá absence sebenších komplikací se získáváním vstupních víz do zemí 
> buržoazní demokracie.

A dokonce autor uvádí, že pokud by nad Stalinem vyhrál Trocki, odevzdal by Sovětský svaz bez boje. A následuje vyústění kapitoly

> Je načase nalít si čistého vína. Hitlera nestvořil Stalin ani němečtí průmyslníci, ale věční geopolitiční rivalové Ruska.
> (str. 100, tučně)










 
